require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mafia371
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.active_job.queue_adapter = :sucker_punch

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.google_key = '869779328334-60e0vm2f6cl6mu7idf39rn1sshts0a2m.apps.googleusercontent.com'
    config.google_secret = 'PmgUICSemdFy7oM8747RhC54'
    config.google_domain = 'https://accounts.google.com/o/'

  end
end
