Rails.application.routes.draw do
  get 'data_explorer/table_of_contents'
  get 'data_explorer/pie'
  get 'data_explorer/age'
  get 'data_explorer/access'
  get 'data_explorer/live'
  get 'data_explorer/comfortability'
  get 'data_explorer/usage'
  get 'data_explorer/lessons'
  get 'data_explorer/methods'
  get 'team_data/load' => 'team_data#load'

   devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
   resources :participants
   resources :uncomfortable_participants

#   root to: "participants#index"
    root to: "data_explorer#table_of_contents"  
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
