class CreateUncomfortableParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :uncomfortable_participants do |t|
      t.references :participant, foreign_key: true
      t.boolean :skills_for_improvement
      t.text :first_skill
      t.text :reason_behind_first_skill

      t.timestamps
    end
  end
end
