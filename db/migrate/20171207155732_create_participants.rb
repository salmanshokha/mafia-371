class CreateParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :participants do |t|
      t.boolean :live_in_korea
      t.integer :age
      t.boolean :have_internet_access
      t.integer :usage_frequency
      t.text :reason_for_using
      t.boolean :comfortability
      t.boolean :lessons_taken
      t.text :benefitted_how
      t.text :activity_list

      t.timestamps
    end
  end
end
