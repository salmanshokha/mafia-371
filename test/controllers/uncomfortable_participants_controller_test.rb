require 'test_helper'

class UncomfortableParticipantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @uncomfortable_participant = uncomfortable_participants(:one)
  end

  test "should get index" do
    get uncomfortable_participants_url
    assert_response :success
  end

  test "should get new" do
    get new_uncomfortable_participant_url
    assert_response :success
  end

  test "should create uncomfortable_participant" do
    assert_difference('UncomfortableParticipant.count') do
      post uncomfortable_participants_url, params: { uncomfortable_participant: { first_skill: @uncomfortable_participant.first_skill, participant_id: @uncomfortable_participant.participant_id, reason_behind_first_skill: @uncomfortable_participant.reason_behind_first_skill, skills_for_improvement: @uncomfortable_participant.skills_for_improvement } }
    end

    assert_redirected_to uncomfortable_participant_url(UncomfortableParticipant.last)
  end

  test "should show uncomfortable_participant" do
    get uncomfortable_participant_url(@uncomfortable_participant)
    assert_response :success
  end

  test "should get edit" do
    get edit_uncomfortable_participant_url(@uncomfortable_participant)
    assert_response :success
  end

  test "should update uncomfortable_participant" do
    patch uncomfortable_participant_url(@uncomfortable_participant), params: { uncomfortable_participant: { first_skill: @uncomfortable_participant.first_skill, participant_id: @uncomfortable_participant.participant_id, reason_behind_first_skill: @uncomfortable_participant.reason_behind_first_skill, skills_for_improvement: @uncomfortable_participant.skills_for_improvement } }
    assert_redirected_to uncomfortable_participant_url(@uncomfortable_participant)
  end

  test "should destroy uncomfortable_participant" do
    assert_difference('UncomfortableParticipant.count', -1) do
      delete uncomfortable_participant_url(@uncomfortable_participant)
    end

    assert_redirected_to uncomfortable_participants_url
  end
end
