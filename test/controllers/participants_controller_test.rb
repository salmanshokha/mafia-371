require 'test_helper'

class ParticipantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @participant = participants(:one)
  end

  test "should get index" do
    get participants_url
    assert_response :success
  end

  test "should get new" do
    get new_participant_url
    assert_response :success
  end

  test "should create participant" do
    assert_difference('Participant.count') do
      post participants_url, params: { participant: { activity_list: @participant.activity_list, age: @participant.age, benefitted_how: @participant.benefitted_how, comfortability: @participant.comfortability, have_internet_access: @participant.have_internet_access, lessons_taken: @participant.lessons_taken, live_in_korea: @participant.live_in_korea, reason_for_using: @participant.reason_for_using, usage_frequency: @participant.usage_frequency } }
    end

    assert_redirected_to participant_url(Participant.last)
  end

  test "should show participant" do
    get participant_url(@participant)
    assert_response :success
  end

  test "should get edit" do
    get edit_participant_url(@participant)
    assert_response :success
  end

  test "should update participant" do
    patch participant_url(@participant), params: { participant: { activity_list: @participant.activity_list, age: @participant.age, benefitted_how: @participant.benefitted_how, comfortability: @participant.comfortability, have_internet_access: @participant.have_internet_access, lessons_taken: @participant.lessons_taken, live_in_korea: @participant.live_in_korea, reason_for_using: @participant.reason_for_using, usage_frequency: @participant.usage_frequency } }
    assert_redirected_to participant_url(@participant)
  end

  test "should destroy participant" do
    assert_difference('Participant.count', -1) do
      delete participant_url(@participant)
    end

    assert_redirected_to participants_url
  end
end
