class DataExplorerController < ApplicationController
  def age
	# pull and expose the data
	@data = Participant.group( :age ).count.to_a
  end

  def usage
        # pull and expose the data
        @data = Participant.group( :usage_frequency ).count.to_a
  end

  def live
        # pull and expose the data
        @data = Participant.group( :live_in_korea ).count.to_a
  end

  def access
        # pull and expose the data
        @data = Participant.group( :have_internet_access ).count.to_a
  end

  def comfortability
        # pull and expose the data
        @data = Participant.group( :comfortability ).count.to_a
  end

  def lessons
        # pull and expose the data
        @data = Participant.group( :lessons_taken ).count.to_a
  end

  def histogram
        # pull and expose the data
        @data = Participant.group( :age ).count.to_a
  end
end
