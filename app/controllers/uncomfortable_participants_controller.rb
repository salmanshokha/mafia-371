class UncomfortableParticipantsController < ApplicationController
  before_action :set_uncomfortable_participant, only: [:show, :edit, :update, :destroy]

  # GET /uncomfortable_participants
  # GET /uncomfortable_participants.json
  def index
    @uncomfortable_participants = UncomfortableParticipant.all
  end

  # GET /uncomfortable_participants/1
  # GET /uncomfortable_participants/1.json
  def show
  end

  # GET /uncomfortable_participants/new
  def new
    @uncomfortable_participant = UncomfortableParticipant.new
  end

  # GET /uncomfortable_participants/1/edit
  def edit
  end

  # POST /uncomfortable_participants
  # POST /uncomfortable_participants.json
  def create
    @uncomfortable_participant = UncomfortableParticipant.new(uncomfortable_participant_params)

    respond_to do |format|
      if @uncomfortable_participant.save
        format.html { redirect_to @uncomfortable_participant, notice: 'Uncomfortable participant was successfully created.' }
        format.json { render :show, status: :created, location: @uncomfortable_participant }
      else
        format.html { render :new }
        format.json { render json: @uncomfortable_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /uncomfortable_participants/1
  # PATCH/PUT /uncomfortable_participants/1.json
  def update
    respond_to do |format|
      if @uncomfortable_participant.update(uncomfortable_participant_params)
        format.html { redirect_to @uncomfortable_participant, notice: 'Uncomfortable participant was successfully updated.' }
        format.json { render :show, status: :ok, location: @uncomfortable_participant }
      else
        format.html { render :edit }
        format.json { render json: @uncomfortable_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uncomfortable_participants/1
  # DELETE /uncomfortable_participants/1.json
  def destroy
    @uncomfortable_participant.destroy
    respond_to do |format|
      format.html { redirect_to uncomfortable_participants_url, notice: 'Uncomfortable participant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_uncomfortable_participant
      @uncomfortable_participant = UncomfortableParticipant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def uncomfortable_participant_params
      params.require(:uncomfortable_participant).permit(:participant_id, :skills_for_improvement, :first_skill, :reason_behind_first_skill)
    end
end
