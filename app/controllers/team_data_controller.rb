﻿class TeamDataController < ApplicationController
	def load
		  	  current_user.refresh_token_if_expired
		    	session = GoogleDrive::Session.from_access_token(current_user.token)
				#Get
		sheet = session.spreadsheet_by_title('371 Survey Responses').worksheet_by_title('Responses')
				(2..sheet.num_rows).each do |row_num|
			live_in_korea = sheet[row_num,3]
			age = sheet[row_num,4]
			have_internet_access = sheet[row_num,5]
			usage_frequency = sheet[row_num, 6]
			comfortability = sheet[row_num, 9]
			lessons_taken = sheet[row_num, 14]
			reason_for_using = sheet[row_num, 7]
			benefitted_how = sheet[row_num, 13]
			activity_list = sheet[row_num, 8]
                        skills_for_improvement = sheet[row_num, 10]
                        first_skill = sheet[row_num, 11] 
   		  	reason_behind_first_skill = sheet[row_num, 12]		


			# Do you live in Korea?	
			live_in_korea_processed = false
			if live_in_korea == 'Yes'
				live_in_korea = true
			end

			# Which age group do you belong to?	
			age_processed = 0
			case age
			when 'Below 20'
				age_processed = 15
			when '21 - 30 years'
				age_processed = 25
			when '31 - 40 years'
				age_processed = 35
			when '41 - 50 years'
				age_processed = 45
			when '51 - 60 years'
				age_processed = 55
			when 'More than 60 years'
				age_processed = 65
			end

		# Do you have access to a computer, a tablet, or a smartphone with an active internet connection?		
			have_internet_access_processed = false
			if have_internet_access == 'Yes'
				have_internet_access_processed = true
			end

		# How often do you use the Internet?  
			usage_frequency_processed = 0
			case usage_frequency
			when 'Everyday'
				usage_frequency_processed = 5
			when 'Once a week'
				usage_frequency_processed = 4
			when 'Once a month'
				usage_frequency_processed = 3
			when 'Less than once in a month'
				usage_frequency_processed = 2
			when 'Rarely'
				usage_frequency_processed = 1
			end

		# Are you comfortable doing all the activities listed in the previous question?    
			comfortability_processed = false
			if comfortability == 'Yes, I am equally comfortable with all of these.'
				comfortability_processed = true
			end
		# Have you ever taken lessons to improve any of these skills?
			lessons_taken_processed = false
			if lessons_taken == 'Yes, I have.'
				lessons_taken_processed = true
			end

		# Which of the following skills do you think you need to improve on?
		        skills_for_improvement_processed = true
			if skills_for_improvement == ' '
				skills_for_improvement_processed = false
			end

		# Create the Child Objects
			
			uncomfortable_participant = UncomfortableParticipant.create(
			skills_for_improvement: skills_for_improvement_processed,
			first_skill: first_skill,
			reason_behind_first_skill: reason_behind_first_skill
)


			# Assemble the Participant
			participants = Participant.create(
				live_in_korea: live_in_korea,
				age: age_processed, 
				have_internet_access: have_internet_access_processed, 
				usage_frequency: usage_frequency_processed,
				comfortability: comfortability_processed, 
				uncomfortable_participant: uncomfortable_participant,
				lessons_taken: lessons_taken_processed,
				benefitted_how: benefitted_how,
				reason_for_using: reason_for_using,
				activity_list: activity_list	
	)

		end    
	end
end
