json.extract! uncomfortable_participant, :id, :participant_id, :skills_for_improvement, :first_skill, :reason_behind_first_skill, :created_at, :updated_at
json.url uncomfortable_participant_url(uncomfortable_participant, format: :json)
