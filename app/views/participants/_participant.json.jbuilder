json.extract! participant, :id, :live_in_korea, :age, :have_internet_access, :usage_frequency, :reason_for_using, :comfortability, :lessons_taken, :benefitted_how, :activity_list, :created_at, :updated_at
json.url participant_url(participant, format: :json)
